# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.2

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 1.1.1

- patch: Internal maintenance: Add check for newer version.

## 1.1.0

- minor: The pipe now accepts directory path in RESOURCE_PATH variable

## 1.0.0

- major: Changes the SPEC_FILE varialbe to RESOURCE_PATH

## 0.1.4

- patch: Internal maintenance: Update pipe's base Docker image to google/cloud-sdk:273.0.0-slim.

## 0.1.3

- patch: Documentation updates

## 0.1.2

- patch: Fixed the pipe metadata

## 0.1.1

- patch: Internal maintenance: Update dependencies.

## 0.1.0

- minor: Initial release

